#include <iostream>
#include <vector>
#include <string>
#include "colorboard.h"
#include "samegame.h"

Coordinate parse_move(std::string &input);

int main() {
    int score = 0, result;
    Board board(9, std::vector<char>(9));
    generate_board(board);
    print_board(board, score);
    do {
        std::string input;
        std::cin >> input;
        Coordinate move = parse_move(input);
        result = make_move(board, move);
        if(result == 0) {
            std::cerr << "Invalid move!" << std::endl;
            continue;
        }
        score += result;
        print_board(board, score);
    } while (has_valid_move(board));
    std::cout << "Score: " << score << std::endl;
    return 0;
}

Coordinate parse_move(std::string &input) {
    if(input[0] >= 97) {
        return {(char)(input[1] - 48), (char)(input[0] - 97)};
    }
    return {(char)(input[1] - 48), (char)(input[0] - 65)};
}
