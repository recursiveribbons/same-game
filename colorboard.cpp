#include <iostream>
#include <random>
#include "colorboard.h"

void print_row_num(int i);

void print_color(const char &color);

void reset_color();

char map_color(char c);

void generate_board(Board &board) {
    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<> dis(0, 5);
    for (std::vector<char> &row : board) {
        for (char &field : row) {
            field = map_color((char) dis(gen));
        }
    }

}

void print_board(const Board &board, int score) {
    std::cout << "Current score: " << score << std::endl;
    std::cout << "  A B C D E F G H I" << std::endl;
    int i = 0;
    for (const std::vector<char> &row : board) {
        print_row_num(i++);
        for (const char &field : row) {
            print_color(field);
        }
        std::cout << std::endl;
    }
    reset_color();
}

void print_row_num(int i) {
    reset_color();
    std::cout << i;
}

void print_color(const char &color) {
    if (color != EMPTY) {
        printf(" %c[%dm*", 0x1B, color);
    } else {
        std::cout << "  ";
    }
}

void reset_color() {
    printf("%c[0m", 0x1B);
}

char map_color(char c) {
    switch (c) {
        case 0:
            return RED;
        case 1:
            return GREEN;
        case 2:
            return YELLOW;
        case 3:
            return BLUE;
        case 4:
            return MAGENTA;
        default:
            return RED;
    }
}
