#include <algorithm>
#include "colorboard.h"
#include "samegame.h"

bool has_neighbors(const Board &board, Coordinate c);

void find_adjacent(const Board &board, char i, char j, std::vector<Coordinate> &list);

bool is_in_list(const std::vector<Coordinate> &list, Coordinate value);

void remove_blocks(Board &board, std::vector<Coordinate> &list);

void block_drop(Board &board);

void column_constrict(Board &board);

bool column_empty(const Board &board, int j);

bool has_valid_move(const Board &board) {
    for (char i = 0; i < 9; i++) {
        for (char j = 0; j < 9; j++) {
            if (has_neighbors(board, {i, j})) {
                return true;
            }
        }
    }
    return false;
}

int make_move(Board &board, Coordinate c) {
    if (c.i > 8 || c.j > 8 || c.i < 0 || c.j < 0) {
        return 0;
    }
    if (board[c.i][c.j] == EMPTY || !has_neighbors(board, c)) {
        return 0;
    }
    std::vector<Coordinate> list;
    find_adjacent(board, c.i, c.j, list);
    remove_blocks(board, list);
    block_drop(board);
    column_constrict(board);
    int n = list.size();
    return n * (n - 1);
}

bool has_neighbors(const Board &board, Coordinate c) {
    int i = c.i;
    int j = c.j;
    if (board[i][j] == EMPTY) {
        return false;
    }
    return (i < 8 && board[i + 1][j] == board[i][j]) || (j < 8 && board[i][j + 1] == board[i][j]) ||
           (i > 0 && board[i - 1][j] == board[i][j]) || (j > 0 && board[i][j - 1] == board[i][j]);
}

void find_adjacent(const Board &board, char i, char j, std::vector<Coordinate> &list) {
    if (is_in_list(list, {i, j})) {
        return;
    }
    list.push_back({i, j});
    if (i < 8 && board[i + 1][j] == board[i][j]) {
        find_adjacent(board, i + 1, j, list);
    }
    if (j < 8 && board[i][j + 1] == board[i][j]) {
        find_adjacent(board, i, j + 1, list);
    }
    if (i > 0 && board[i - 1][j] == board[i][j]) {
        find_adjacent(board, i - 1, j, list);
    }
    if (j > 0 && board[i][j - 1] == board[i][j]) {
        find_adjacent(board, i, j - 1, list);
    }
}

bool is_in_list(const std::vector<Coordinate> &list, Coordinate value) {
    for (const Coordinate &c : list) {
        if (c.i == value.i && c.j == value.j) {
            return true;
        }
    }
    return false;
}

void remove_blocks(Board &board, std::vector<Coordinate> &list) {
    for (const Coordinate &c : list) {
        board[c.i][c.j] = EMPTY;
    }
}

void block_drop(Board &board) {
    for (int j = 0; j < 9; j++) {
        for (int i = 8; i > 0; i--) {
            int k = i;
            while (k <= 8 && board[k][j] == EMPTY && board[k - 1][j] != EMPTY) {
                std::swap(board[k][j], board[k - 1][j]);
                k++;
            }
        }
    }
}

void column_constrict(Board &board) {
    for (int j = 1; j < 9; j++) {
        int k = j;
        while (!column_empty(board, k) && column_empty(board, k - 1) && k >= 1) {
            for (int i = 0; i < 9; i++) {
                std::swap(board[i][k], board[i][k - 1]);
            }
            k--;
        }
    }
}

bool column_empty(const Board &board, int j) {
    for (int i = 0; i < 9; i++) {
        if (board[i][j] != EMPTY) {
            return false;
        }
    }
    return true;
}
