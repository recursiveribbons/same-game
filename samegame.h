#ifndef SAMEGAME_SAMEGAME_H
#define SAMEGAME_SAMEGAME_H

bool has_valid_move(const Board &board);

int make_move(Board &board, Coordinate c);

#endif //SAMEGAME_SAMEGAME_H
